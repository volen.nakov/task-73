import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [count, setCount] = useState(1)
  useEffect(() => {
    document.title=count
  }, [count])
  return (
    <div className="App">
      <button onClick={() => { setCount(count + 1) }}>Count: ({count})</button>
    </div>
  );
}

export default App;
